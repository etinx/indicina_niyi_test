
const { isSet } = require('lodash');
require('../models/Models.js');
let config = require('../configs');
mongoose = require('mongoose'),
    Url = mongoose.model('Url'),
    UrlStat = mongoose.model('UrlStat'),
    path = require('path'),
    //fs = require('fs'),
    { customAlphabet } = require('nanoid'),
    nanoid = customAlphabet('1234567890abcdefghijklmnopqrstuvwxyz', 6),
    validator = require('validator'),
    checks = require('lodash');




module.exports.encode = function (req, res) {
    if (isSet_(req.body.originalUrl) && isValidUrl(req.body.originalUrl)) {
        makeShortUrl(req.body.originalUrl.toLowerCase(), res);
    } else {
        ClientErrorResponse.error_string = "Error! Invalid url provided. Please retry"
        res.status(SERVER_ERROR_RESPONSE_CODE).json(ServerErrorResponse)
    }

}


module.exports.decode = function (req, res) {
    let isValidUrl = false;
    if (process.env.NODE_ENV === "production") {
        isValidUrl = isSet_(req.body.shortUrl) && isValidUrl(req.body.shortUrl)
    } else {
        isValidUrl = isSet_(req.body.shortUrl) && !req.body.shortUrl.isEmpty()
    }
    if (isValidUrl) {
        getOriginalUrl(req.body.shortUrl.toLowerCase(), res);
    } else {
        ClientErrorResponse.error_string = "Error! Invalid url provided. Please retry"
        res.status(SERVER_ERROR_RESPONSE_CODE).json(ClientErrorResponse)
    }
}

module.exports.statistic = function (req, res) {
    if (isSet_(req.params.url_path)) {
        let key = req.params.url_path;
        loadStats(key, res)
    }
    else {
        ServerErrorResponse.error_string = "Error! Invalid parameters provided. Please retry"
        res.status(CLIENT_ERROR_RESPONSE_CODE).json(ServerErrorResponse)
    }
}

module.exports.list = function (req, res) {
    if (isSet_(req.body.page) && !isNaN(req.body.page) && parseInt(req.body.page) > 0) {
        loadUrls(req.body.page, res);
    } else {
        loadUrls(1, res);
    }
}

module.exports.redirectToUrl = async function (req, res) {
    if (isSet_(req.params.url_path)) {
        let shortUrl = req.params.url_path
        loadUrl(shortUrl, res)
    } else {
        ServerErrorResponse.error_string = "Error! Invalid parameters provided. Please retry"
        res.status(CLIENT_ERROR_RESPONSE_CODE).json(ServerErrorResponse)
    }
}


const loadUrl = async (shortId, res) => {
    let checkUrl = await Url.findOne({
        shortId
    })
    console.log(checkUrl)
    if (checkUrl) {
        res.writeHead(301, { "Location": checkUrl.originalUrl });
        return res.end();
    } else {
        ServerErrorResponse.error_string = 'Error! Provided url not found'
        res.status(CLIENT_ERROR_RESPONSE_CODE).json(ServerErrorResponse)
    }

}

const makeShortUrl = async (originalUrl, res) => {
    try {
        let uniqId = await nanoid()
        let checkUrl = await Url.findOne({
            originalUrl
        })
        if (checkUrl) {
            SuccessResponse.response_string = 'Url shortened successfully'
            SuccessResponse.data = checkUrl
            res.status(SUCCESS_RESPONSE_CODE).json(SuccessResponse)
        } else {

            let originalUrlLength = originalUrl.length
            let shortUrl = process.env.LIVE_APP_BASE_URL + uniqId
            let shortId = uniqId
            let shortUrlLength = shortUrl.length
            let lentDif = originalUrlLength - shortUrlLength;
            let changeType = (lentDif > 0) ? "-" : "+";
            let percentageChange = ((Math.abs(lentDif) / originalUrlLength) * 100).toFixed(2) + "%";
            let url = new Url({
                originalUrl,
                originalUrlLength,
                shortId,
                shortUrl,
                shortUrlLength,
                percentageChange,
                changeType,
                date: new Date()
            })
            await url.save()
            SuccessResponse.response_string = 'Url shortened successfully'
            SuccessResponse.data = url
            res.status(SUCCESS_RESPONSE_CODE).json(SuccessResponse)
        }
    } catch (e) {
        res.status(SERVER_ERROR_RESPONSE_CODE).json(ServerErrorResponse)
    }
}

const getOriginalUrl = async (shortUrl, res) => {
    //simple logic to denote access countries for record purposes
    let locationArray = new Array("Nigeria", "United Kingdom", "Ghana", "Canada", "Kenya")
    let randomIndex = Math.floor(Math.random() * 6);
    randomIndex = (randomIndex > 5) ? randomIndex - 1 : randomIndex;
    try {
        let checkUrl = await Url.findOne({
            shortUrl
        })
        if (checkUrl) {
            let urlId = checkUrl._id
            let Location = locationArray[randomIndex]
            let shortId = checkUrl.shortId
            let dateString = convertUnixToDateString()
            let urlStat = new UrlStat({
                urlId,
                shortId,
                Location,
                date: new Date(),
                dateString
            })

            await urlStat.save()
            SuccessResponse.response_string = 'Url decoded successfully'
            SuccessResponse.data = checkUrl
            res.status(SUCCESS_RESPONSE_CODE).json(SuccessResponse)
        } else {
            ServerErrorResponse.error_string = 'Error! Provided url not found'
            res.status(CLIENT_ERROR_RESPONSE_CODE).json(ServerErrorResponse)
        }
    } catch (e) {
        res.status(SERVER_ERROR_RESPONSE_CODE).json(ServerErrorResponse)
    }
}

const loadUrls = async (page, res) => {
    let skip = (parseInt(page) - 1) * parseInt(process.env.PER_PAGE);

    try {
        let urls = await Url.find({}).sort({ createdAt: -1 }).skip(0).limit(parseInt(process.env.PER_PAGE))
        if (urls) {
            SuccessResponse.response_string = 'Urls loaded successfully'
            SuccessResponse.count = urls.length
            SuccessResponse.data = urls
            res.status(SUCCESS_RESPONSE_CODE).json(SuccessResponse)
        } else {
            ServerErrorResponse.error_string = 'Error! Provided url not found'
            res.status(CLIENT_ERROR_RESPONSE_CODE).json(ServerErrorResponse)
        }
    } catch (e) {
        res.status(SERVER_ERROR_RESPONSE_CODE).json(ServerErrorResponse)
    }
}

const loadStats = async (shortId, res) => {
    try {
        let dateLocationData = await UrlStat.aggregate(
            [
                { $match: { "$and": [{ shortId: shortId }] } },
                { $group: { _id: { dateString: "$dateString", Location: "$Location" }, total: { $sum: 1 } } }
            ]);

        let dateOnly = await UrlStat.aggregate(
            [
                { $match: { "$and": [{ shortId: shortId }] } },
                { $group: { _id: "$dateString", total: { $sum: 1 } } }
            ]);

        let locationOnly = await UrlStat.aggregate(
            [
                { $match: { "$and": [{ shortId: shortId }] } },
                { $group: { _id: "$Location", total: { $sum: 1 } } }
            ]);

        SuccessResponse.response_string = "Success! statistics loaded successfully!"
        SuccessResponse.url_path = shortId
        SuccessResponse.data.dateLocationData = dateLocationData
        SuccessResponse.data.dateOnly = dateOnly
        SuccessResponse.data.locationOnly = locationOnly
        res.status(SUCCESS_RESPONSE_CODE).json(SuccessResponse)
    } catch (e) {
        ServerErrorResponse.error_string = 'Error! Provided url not found'
        res.status(CLIENT_ERROR_RESPONSE_CODE).json(ServerErrorResponse)
    }

}



function isSet_(item) {//
    return (typeof item !== "undefined")
}

function isValidUrl(string) {
    return string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
}

String.prototype.isEmpty = function () {
    return (this.length === 0 || !this.trim());
}

function convertUnixToDateString() {
    let unixtimestamp = Math.round(new Date().getTime() / 1000);
    let date = new Date(unixtimestamp * 1000);
    let year = date.getFullYear();
    let month = date.getMonth() + 1
    let day = date.getDate();

    let convdataTime = day + "-" + month + "-" + year;
    return convdataTime;
}