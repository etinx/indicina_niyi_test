module.exports = {
    secret: process.env.NODE_ENV === 'production' ? process.env.SECRET : 'secret'
};

global.SUCCESS_RESPONSE_CODE = 200;
global.SERVER_ERROR_RESPONSE_CODE = 500;
global.CLIENT_ERROR_RESPONSE_CODE = 400;

//global.DEV_URL =  "http://localhost:3002/";
//global.LIVE_URL = "https://shisastaging.herokuapp.com/";//"https://shisadone.herokuapp.com/";
//global.LIVE_URL = "https://stivesapp.herokuapp.com/";//"https://shisadone.herokuapp.com/";
//global.MONGO_DB_LIVE_URL = 'mongodb+srv://niyious:salaudeen@cluster0-dg3rp.mongodb.net/indicina1?retryWrites=true&w=majority'
//global.MONGO_DB_DEV_URL  = 'mongodb+srv://niyious:salaudeen@cluster0-dg3rp.mongodb.net/indicina1?retryWrites=true&w=majority'
//global.mainToken  = "7ec72580b4dfde87c829ce408aa3192c41e02aac"
//global.LIVE_APP_BAE_URL = "http://localhost:3002/";

global.ClientErrorResponse = {
    'code': CLIENT_ERROR_RESPONSE_CODE,
    'status': false,
    //'invalid_credentials_string': 'Invalid credentials provided. please retry action',
    'error_string': 'Invalid data provided. Please retry your action',
    'errors': {}
};

global.ServerErrorResponse = {
    'code': SERVER_ERROR_RESPONSE_CODE,
    'status': false,
    'error_string': 'Invalid request data. Please retry action',
    'errors': {}
};
//
global.SuccessResponse = {
    'code': SUCCESS_RESPONSE_CODE,
    'status': true,
    'response_string': '',
    'data': {}
};