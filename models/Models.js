const mongoose = require('mongoose');
const { boolean } = require('@hapi/joi');
Schema = mongoose.Schema
crypto = require('crypto')


const UrlSchema = new Schema({
    originalUrl: { type: String, default: "", unique:true },
    originalUrlLength: { type: Number, default: 0 },
    shortId: { type: String, default: "" },
    shortUrl: { type: String, default: "", unique:true },
    shortUrlLength: { type: Number, default: 0 },
    percentageChange: { type: String, default: "" },
    changeType: { type: String, default: "+" },
    usageCount: { type: Number, default: 0 },
    date: {
        type: String,
        default: Date.now
    }
}, { timestamps: true });

UrlSchema.methods.createUrl = function (originalUrl, originalUrlLength, shortUrl, shortUrlLength, percentageChange, changeType) 
{
    this.originalUrl = originalUrl
    this.originalUrlLength = originalUrlLength
    this.shortUrl = shortUrl
    this.shortUrlLength = shortUrlLength
    this.percentageChange = percentageChange
    this.changeType = changeType
}
mongoose.model('Url', UrlSchema)
/*-------------------------------------------------------------*/
const UrlStatSchema = new Schema({
    urlId: { type: mongoose.Schema.Types.ObjectId, ref: 'Url', required: true },
    shortId:{type: String},
    date: {
        type: String,
        default: Date.now
    },
    dateString:{type: String},
    Location: {type: String}
}, { timestamps: true });
mongoose.model('UrlStat', UrlStatSchema)
