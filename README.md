### Set up Instruction
- Clone the repo
- To set up with only NodeJS app in Docker and external MongoDB
```json
Edit MONGO_DB_LIVE_URL & MONGO_DB_DEV_URL with your desired MongoDB instance url
run "docker build -t niyi_indicina" from the console and wait until the app starts up
Visit the url to comfirm app is running
```
- To set up with both NodeJS app & MongoDB in Docker 
```json
Edit MONGO_DB_LIVE_URL & MONGO_DB_DEV_URL with the MongoDB url(example is given in code and it is dependent on the exposed ports)
run "docker-compose up" from the console and wait until the app starts up
Visit the url to comfirm app is running
```
# APIs
-  /api/encode:  Encodes a URL to a shortened URL and returns the shortened url + details. See sample response below:
Methos: POST
parameter: originalUrl (https://punchng.com/scotland-vs-israel-mctominay-gives-scotland-dramatic-win/)

```json
{
    "code": 200,
    "status": true,
    "response_string": "Url shortened successfully",
    "data": {
        "originalUrl": "https://serflow.com/quions49707/push-items-into-ddd",
        "originalUrlLength": 51,
        "shortId": "rgqfsn",
        "shortUrl": "http://localhost:3002/rgqfsn",
        "shortUrlLength": 28,
        "percentageChange": "45.10%",
        "changeType": "-",
        "usageCount": 0,
        "_id": "6161ede3ad1fa407145a139b",
        "date": "Sat Oct 09 2021 20:30:43 GMT+0100 (West Africa Standard Time)",
        "createdAt": "2021-10-09T19:30:43.439Z",
        "updatedAt": "2021-10-09T19:30:43.439Z",
        "__v": 0
    }
}
```
-  /api/decode:  Decodes a URL to the original URL. See sample success response below:
Method: GET
parameter: shortUrl (http://localhost:3002/bpjb0y)

```json
{
    "code": 200,
    "status": true,
    "response_string": "Url loaded successfully",
    "data": {
        "originalUrl": "https://stackoverflow.com/questions/52837106/how-to-use-async-await-with-mongoose-aggregate",
        "originalUrlLength": 63,
        "shortId": "bpjb0y",
        "shortUrl": "http://localhost:3002/bpjb0y",
        "shortUrlLength": 28,
        "percentageChange": "55.56%",
        "changeType": "-",
        "usageCount": 0,
        "_id": "61600b4f9342ba324454da5d",
        "date": "Fri Oct 08 2021 10:11:43 GMT+0100 (West Africa Standard Time)",
        "createdAt": "2021-10-08T09:11:43.457Z",
        "updatedAt": "2021-10-08T09:11:43.457Z",
        "__v": 0
    },
    "count": 6
}
```
-  /api/statistic/{url_path}:  Returns basic statistics a URL. See sample success response below:
Method: GET
parameter: None
```json
{
    "code": 200,
    "status": true,
    "response_string": "Success! statistics loaded successfully!",
    "data": {
        "dateLocationData": [
            {
                "_id": {
                    "dateString": "8-10-2021",
                    "Location": "Ghana"
                },
                "total": 3
            },
            {
                "_id": {
                    "dateString": "7-10-2021",
                    "Location": "United Kingdom"
                },
                "total": 1
            },
            {
                "_id": {
                    "dateString": "9-10-2021",
                    "Location": "United Kingdom"
                },
                "total": 1
            },
            {
                "_id": {
                    "dateString": "8-10-2021",
                    "Location": "Nigeria"
                },
                "total": 6
            },
            {
                "_id": {
                    "dateString": "8-10-2021",
                    "Location": "Kenya"
                },
                "total": 1
            },
            {
                "_id": {
                    "dateString": "8-10-2021",
                    "Location": "United Kingdom"
                },
                "total": 3
            },
            {
                "_id": {
                    "dateString": "8-10-2021",
                    "Location": "Canada"
                },
                "total": 4
            },
            {
                "_id": {
                    "dateString": "8-10-2021"
                },
                "total": 1
            },
            {
                "_id": {
                    "dateString": "7-10-2021",
                    "Location": "Ghana"
                },
                "total": 2
            }
        ],
        "dateOnly": [
            {
                "_id": "7-10-2021",
                "total": 3
            },
            {
                "_id": "9-10-2021",
                "total": 1
            },
            {
                "_id": "8-10-2021",
                "total": 18
            }
        ],
        "locationOnly": [
            {
                "_id": "Canada",
                "total": 4
            },
            {
                "_id": null,
                "total": 1
            },
            {
                "_id": "Nigeria",
                "total": 6
            },
            {
                "_id": "Kenya",
                "total": 1
            },
            {
                "_id": "United Kingdom",
                "total": 5
            },
            {
                "_id": "Ghana",
                "total": 5
            }
        ]
    },
    "url_path": "bpjb0y"
}
```
-  /api/list/:  List all available url
Method: GET
parameter: None

-  /{url_path}:  Redirects to origina URL
Method: GET
parameter: None

### NOTE
- Some enhancements (eg API authentication and more) and security measures should be put in place in other to deploy this solution