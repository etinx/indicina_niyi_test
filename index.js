process.env.TZ = "Africa/Lagos";
require('dotenv').config();
const express = require('express'),
    path = require('path'),
    helmet = require('helmet'),
    cookieParser = require('cookie-parser'),
    bodyParser = require("body-parser"),
    pageRoutes = require('./routes/pages'),
    apiRoutes = require('./routes/api'),
    cors = require('cors'),
    mongoose = require('mongoose'),
    errorhandler = require('errorhandler'),
    cluster = require('cluster'),
    totalCPUs = require('os').cpus().length,
    isProduction = process.env.NODE_ENV === 'production'
/*--- Start clustering for optimum performance-------*/
if (cluster.isMaster) {
    console.log(`Number of CPUs is ${totalCPUs}`);
    console.log(`Master ${process.pid} is running`);

    // Fork workers.
    for (let i = 0; i < totalCPUs; i++) {
        cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);
        console.log("Let's fork another worker!");
        cluster.fork();
    });
/*--- Start clustering for increased performance-------*/
} else {
    app = express(),
        mongooseOptions = {
            useNewUrlParser: true,
            useFindAndModify: false,
            useCreateIndex: true,
            useUnifiedTopology: true
        },
        connection = null
    if (isProduction) {
        connection = mongoose.connect(process.env.MONGO_DB_LIVE_URL, mongooseOptions).then(function () {
            initilizeScript(app)
        }).catch(function (err) {
            console.log(err)
        });
    } else {
        app.use(errorhandler());
        connection = mongoose.connect(process.env.MONGO_DB_DEV_URL, mongooseOptions).then(function () {
            initilizeScript(app)
        }).catch(function (err) {
            console.log(err)
        });
    }
}


function initilizeScript(app) {

    app.use(function (req, res, next) {
        res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        next();
    });
    app.use(cors())
    app.use(helmet())
    app.use(cookieParser())
    app.use(bodyParser.urlencoded({
        extended: false
    }));
    app.use(bodyParser.json());

    app.use(require('morgan')('dev'));
    app
        .use(express.static(path.join(__dirname, 'public')))
        /*.use(function(req, res, next) {
            //console.log(JSON.stringify(req.headers.maintoken));
           // console.log(req.get('maintoken'))
            if (!req.get('mainToken') || req.get('mainToken') != mainToken ) {
              return res.status(403).json({ error: 'No credentials sent!' });
            }
            next();
          })*/
        .set('views', path.join(__dirname, 'views'))
        .set('view engine', 'ejs')
    app.use('/', pageRoutes);
    app.use('/api', apiRoutes);
     app.listen(process.env.APP_PORT, () => console.log("Listening on " + process.env.APP_PORT))
}