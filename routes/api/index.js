const routes = require('express').Router();
const userController = require('../../controllers/controller.js');

//const userController = require('../../controllers/controller_mysql_.js');
routes.post('/encode', userController.encode);
routes.get('/decode', userController.decode);
routes.get('/statistic/:url_path?', userController.statistic); 
routes.get('/list', userController.list);


module.exports = routes