const routes = require('express').Router();

let data = { baseUrl: process.env.LIVE_APP_BASE_URL }

const userController = require('../../controllers/controller.js');
routes.get('/', (req, res) =>   res.status(SUCCESS_RESPONSE_CODE).render('pages/index', { baseUrl: process.env.LIVE_APP_BASE_URL }))
routes.get('/:url_path', userController.redirectToUrl);
module.exports = routes;